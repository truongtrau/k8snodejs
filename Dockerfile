# stage 1

FROM node:alpine AS k8s
WORKDIR /app
COPY . .
RUN npm ci && npm run build

# stage 2

FROM nginx:alpine
COPY --from=k8s /app/dist/k8s-ui /usr/share/nginx/html
EXPOSE 80